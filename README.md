# nodejs-chatwork
It's a chatwork module without Official API for Node.js.
So there is no limitaion of number of function call.

## Installation

```npm install nodejs-chatwork```

## Usage

### Initializing
```
var Chatwork = require('nodejs-chatwork');
var cw = new Chatwork();

cw.init({
    email:"{EMAIL}",
    password:"{PASSWORD}",
    oncomplete:function(info){

    },
    onerror:function(err){
    }
});
```
Caution:
This module supports only login with email and password pair.

### Watching chat rooms
```
cw.watch({
   oncomplete:function(){

   },
   onmessage:function(msgObj){
       switch(msgObj.type) {
           case 'messageAdded':
           // A message is added.

           break;
           case 'messageUpdated':
           // A message is updated(edited).

           break;
           case 'messageRemoved':
           // A message is removed.

           break;
           case 'taskAdded':
           // A task is added.

           break;
           case 'taskUpdated':
           // A task is updated(edited).

           break;
           case 'taskDone':
           // A task is done.

           break;
           case 'taskRemoved':
           // A task is removed.

           break;
           case 'roomInvited':
           // you are invited the room.

           break;
           case 'roomKicked':
           // you are kicked from the room or the room is removed.

           break;
           case 'roomInfoChanged':
           // A name or description of the room is changed.

           break;
           case 'roomMemberChanged':
           // members or a role of a member of the room is changed.

           break;
           default:
           // unknown message
       }

   },
   onerror:function(err){

   }
});
```
### Messages Operations
#### Adding a message

```
cw.sendMessage({
    msg:{
        text:"{MESSAGE_TEXT}",
        room_id:{ROOM_ID}
    },
    oncomplete:function(status){

    },
    onerror:function(err){

    }
});
```

#### Gettng a message
```
cw.getMessage({
    message_id:{MESSAGE_ID},
    room_id:{ROOM_ID},
    oncomplete:function(message){

    },
    onerror:function(err){

    }
})
```

### Tasks Operations
#### Adding a task
```
cw.addTask({
    task:{
        body:"{TASK_TEXT}",
        room_id:{ROOM_ID},
        to_ids:[{ASSIGNEE_ID1},{ASSIGNEE_ID2}, ...],
        limit:{
            year:{YEAR_OF_TASKLIMIT},
            month:{MONTH_OF_TASKLIMIT},
            date:{DATE_OF_TASKLIMIT}
        }
    },
    oncomplete:function(status){

    },
    onerror:function(err){

    }
});
```
If a limit property is null or undefined, a limit of new task is set to the day you invoke the method.

#### Getting a task
```
cw.getTask({
    task_id:{TASK_ID},
    room_id:{ROOM_ID},
    oncomplete:function(task){

    },
    onerror:function(err){

    }
});
```

#### Closing a task
```
cw.closeTask({
    task_id:{TASK_ID},
    oncomplete:function(status){

    },
    onerror:function(err){

    }
});
```

#### Getting tasks by status
```
// Getting MY tasks
cw.getMyTask({
    status:"{TASK_STATUS}", // "open" or "done"
    oncomplete:function(tasks){

    },
    onerror:function(err){

    }
});
```

### Room Operations
#### Getting rooms
```
cw.getRooms({
    room_id:{ROOM_ID},
    oncomplete:function(Rooms){

    },
    onerror:function(err){

    }
});
```
#### Getting a Room
```
cw.getRoom({
    room_id:{ROOM_ID},
    oncomplete:function(room){

    },
    onerror:function(err){

    }
});
```
#### Getting Members in a room
```
cw.getMembers({
    room_id:{ROOM_ID},
    oncomplete:function(members){
    },
    onerror:function(err){

    }
});
```

### Account information Operations
#### Getting Detail Account Infomations
```
cw.getDetailAccountInfo({
    aid:{ACCOUNT_ID},
    oncomplete:function(info){

    },
    onerror:function(err){

    }
})
```
#### Getting Detail MY Infomations
```
cw.getDetailMyInfo({
    oncomplete:function(info){

    },
    onerror:function(err){

    }
})
```
#### Getting contacts
```
cw.getContacts({
    oncomplete:function(contacts){

    },
    onerror:function(err) {

    }
});
```

## Unimplemented operations.
* Editing/Deleting messages
* Editing/Deleting tasks
* Creating/Updating/Deleting rooms
* Uploading/Downloading/Deleting files
* Adding/Requesting/Removing contacts
