var Url= require('url'),
    http = require('http'),
    phantom=require('node-phantom-simple'),
    https = require('https'),
    Tunnel = require('tunnel-agent');

var Chatwork = function(){
    var _status = null;
    var _cwinfo = null;

    var request = function(options, handlers) {
        var req = createHttpRequest(options);
        var data = '';
        req.on('response', function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                data+=chunk;
            });
            res.on('end', function () {
                if(handlers[res.statusCode])handlers[res.statusCode](res, data);
                else if(handlers["else"])handlers["else"](res, data);
            }.bind(this));
        }.bind(this));
        req.on("error", function(err) {
            console.log(err);
        }.bind(this));
        if(options.body)req.write(options.body);
        req.end();
    };

    var  readChat = function(params, roomid, lastChatId) {
        var options = {};
        options.url = 'https://www.chatwork.com/gateway.php?cmd=read&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&room_id='+roomid+'&last_chat_id='+lastChatId+'&_='+new Date().getTime().toString();
        options.headers={"Cookie":this._cookies};
        options.method = 'get';
        options.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, options, {
            "200":function(res, data) {

            },
            "else":function(res, data) {

            }
        });
    };

    var  getRoomInfo = function(params, roomid, updateRet, not_tasks) {
        var options = {};
        options.url = 'https://www.chatwork.com/gateway.php?cmd=get_room_info&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja';
        options.headers={"Cookie":this._cookies};
        options.headers['Content-Type'] = "application/x-www-form-urlencoded";
        options.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        options.method = 'post';

//        var body =('{"i":{'+(updateRet.i==1?('"'+roomid+'":{"c":'+(_cwinfo.room_dat[roomid].c?_cwinfo.room_dat[roomid].c:'0')+',"u":'+(_cwinfo.room_dat[roomid].u?_cwinfo.room_dat[roomid].u:'0')+',"l":'+_cwinfo.id+',"f":99,"lf":'+(_cwinfo.room_dat[roomid].finfo?_cwinfo.room_dat[roomid].finfo.id:0)+',"t":'+(_cwinfo.room_dat[roomid].lt?(_cwinfo.room_dat[roomid].lt-(_cwinfo.room_dat[roomid].editinfo?2:0)):parseInt(new Date().getTime()/1000))+'}'):'')+'},"p":['+(updateRet.p==1?'"'+roomid+'"':'')+'],"m":['+(updateRet.m==1?'"'+roomid+'"':'')+'],"d":['+(updateRet.d==1?'"'+roomid+'"':'')+'],"t":{'+(not_tasks==false?'':'"'+roomid+'":1')+'},"rid":0,"type":""}');
        var body =('{"i":{'+(updateRet.i==1?('"'+roomid+'":{"c":'+(_cwinfo.room_dat[roomid].c?_cwinfo.room_dat[roomid].c:'0')+',"u":'+(_cwinfo.room_dat[roomid].u?_cwinfo.room_dat[roomid].u:'0')+',"l":'+_cwinfo.id+',"f":99,"lf":'+(_cwinfo.room_dat[roomid].finfo?_cwinfo.room_dat[roomid].finfo.id:0)+',"t":'+(_cwinfo.room_dat[roomid].lt?(_cwinfo.room_dat[roomid].lt-(_cwinfo.room_dat[roomid].editinfo?2:0)):parseInt(new Date().getTime()/1000))+'}'):'')+'},"p":["'+roomid+'"],"m":["'+roomid+'"],"d":["'+roomid+'"],"t":{"'+roomid+'":1},"rid":0,"type":"","load_file_version":"2"}');
        options.body = 'pdata='+encodeURIComponent(body);

        var lastModified = Math.floor(Date.now()/1000);

        request.call(this, options, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    if(resBody.result.room_dat[roomid]!=null) {
                        var roomName = resBody.result.room_dat[roomid].n;
                        var chat_list = resBody.result.room_dat[roomid].chat_list;
                        if(JSON.stringify(_cwinfo.room_dat[roomid])=='{}') {
                            _cwinfo.room_dat[roomid] = resBody.result.room_dat[roomid];
                            params.onmessage({
                                type:"roomInvited",
                                data:{
                                    room_id: parseInt(roomid),
                                    name: _cwinfo.room_dat[roomid].n,
                                    type: _cwinfo.room_dat[roomid].tp==1?"group":(_cwinfo.room_dat[roomid].tp==2?"direct":"my"),
                                    role: _cwinfo.room_dat[roomid].m[_cwinfo.myid]==1?"admin":(_cwinfo.room_dat[roomid].m[_cwinfo.myid]==2?"member":"readonly"),
                                    sticky: (_cwinfo.room_dat[roomid].s==1),
                                    unread_num: -1,
                                    mention_num: -1,
                                    mytask_num: _cwinfo.room_dat[roomid].mt?_cwinfo.room_dat[roomid].mt:0,
                                    message_num: _cwinfo.room_dat[roomid].c,
                                    file_num: _cwinfo.room_dat[roomid].f,
                                    task_num: _cwinfo.room_dat[roomid].t,
                                    icon_path: _cwinfo.room_dat[roomid].ic,
                                    last_update_time: _cwinfo.room_dat[roomid].lt,
                                    description: _cwinfo.room_dat[roomid].d?_cwinfo.room_dat[roomid].d:""
                                }
                            });
//                        } else if(resBody.result.room_dat[roomid].c!=null){
//                            _cwinfo.room_dat[roomid].c = resBody.result.room_dat[roomid].c;
                        }
                        var roomInfoChanged = null;
                        if(_cwinfo.room_dat[roomid].n&&resBody.result.room_dat[roomid].n&&_cwinfo.room_dat[roomid].n!=resBody.result.room_dat[roomid].n) {
                            roomInfoChanged = {
                                room_id: parseInt(roomid),
                                name: resBody.result.room_dat[roomid].n
                            }
                        }
                        if(_cwinfo.room_dat[roomid].d&&_cwinfo.room_dat[roomid].d!=resBody.result.room_dat[roomid].d) {
                            if(!roomInfoChanged) {
                                roomInfoChanged = {
                                    room_id: parseInt(roomid)
                                }
                            }
                            roomInfoChanged.description = resBody.result.room_dat[roomid].d;
                        }
                        if(roomInfoChanged) {
                            params.onmessage({
                                type:"roomInfoChanged",
                                data:roomInfoChanged
                            });
                        }

                        if(_cwinfo.room_dat[roomid].m&&JSON.stringify(_cwinfo.room_dat[roomid].m)!=JSON.stringify(resBody.result.room_dat[roomid].m)) {
                            var roomMemberChanged = {
                            };
                            for(var bmid in _cwinfo.room_dat[roomid].m) {
                                if(!resBody.result.room_dat[roomid].m[bmid]) {
                                    if(!roomMemberChanged.removed){
                                        roomMemberChanged.removed = {};
                                    }
                                    roomMemberChanged.removed[bmid] = _cwinfo.room_dat[roomid].m[bmid];
                                }else if(resBody.result.room_dat[roomid].m[bmid] != _cwinfo.room_dat[roomid].m[bmid]) {
                                    if(!roomMemberChanged.changed){
                                        roomMemberChanged.changed = {};
                                    }
                                    roomMemberChanged.changed[bmid] = resBody.result.room_dat[roomid].m[bmid]==1?"admin":(resBody.result.room_dat[roomid].m[bmid]==2?"member":"readonly");
                                }
                            }
                            for(var amid in resBody.result.room_dat[roomid].m) {
                                if(!_cwinfo.room_dat[roomid].m[amid]) {
                                    if(!roomMemberChanged.added){
                                        roomMemberChanged.added = {};
                                    }
                                    roomMemberChanged.added[amid] = resBody.result.room_dat[roomid].m[amid];
                                }
                            }
                            params.onmessage({
                                type:"roomMemberChanged",
                                data:roomMemberChanged
                            });
                        }

                        for(var name in resBody.result.room_dat[roomid]) {
                            if(name != 'chat_list') {
                                if(name == 'n' || name == 'd' || name == 'm') {
                                    ///
                                }
                                _cwinfo.room_dat[roomid][name] = resBody.result.room_dat[roomid][name];
                            }
                        }
                        _cwinfo.room_dat[roomid].lt=lastModified;
                        _cwinfo.room_dat[roomid].editinfo=false;
                        if(chat_list) {
                            var id=0;
                            chat_list.forEach(function(chat){
                                if(id<chat.id) {
                                    id = chat.id;
                                }
                                _cwinfo.room_dat[roomid].u=(_cwinfo.myid==chat.aid)?0:_cwinfo.room_dat[roomid].u+1;
                                if(params.onmessage) {
                                    var account = (_cwinfo.contact_dat[chat.aid]?_cwinfo.contact_dat[chat.aid]:(_cwinfo.account_dat[chat.aid]?_cwinfo.account_dat[chat.aid]:null))
                                    var data = {
                                        message_id:parseInt(chat.id),
                                        account:{
                                            account_id:parseInt(chat.aid),
                                            name:account?account.nm:"",
                                            avatar_image_url:account?account.av:""
                                        },
                                        room:{
                                            room_id:parseInt(roomid),
                                            name:_cwinfo.room_dat[roomid]?_cwinfo.room_dat[roomid].n:"",
                                            icon_path:_cwinfo.room_dat[roomid]?_cwinfo.room_dat[roomid].ic:"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                                        },
                                        body:chat.msg,
                                        send_time:chat.tm,
                                        update_time:chat.utm
                                    }
                                    params.onmessage({type:"messageAdded", data:data});
                                }
                            }.bind(this));
                            _cwinfo.id = id;
                            if(!params.unread) {
                                readChat.call(this, params, roomid, id);
                            }
                        }
                        if(resBody.result.room_dat[roomid].task_dat) {
                            var added_tasks=[];
                            var tasks;
                            tasks = resBody.result.room_dat[roomid].task_dat;
                            for(var task_id in tasks) {
                                if(!_cwinfo.room_dat[roomid].task_dat[task_id]){
                                    var task = tasks[task_id];
                                    var room = _cwinfo.room_dat[roomid];
                                    var account = _cwinfo.contact_dat[task.aid]?_cwinfo.contact_dat[task.aid]:(_cwinfo.account_dat[task.aid]?_cwinfo.account_dat[task.aid]:null);
                                    var assigner = _cwinfo.contact_dat[task.bid]?_cwinfo.contact_dat[task.bid]:(_cwinfo.account_dat[task.bid]?_cwinfo.account_dat[task.bid]:null);
                                    var data={
                                        task_id:parseInt(task_id),
                                        account:{
                                            account_id:parseInt(account.aid),
                                            name:account?account.nm:"",
                                            avatar_image_url:account?account.av:""
                                        },
                                        room:{
                                            room_id:parseInt(roomid),
                                            name:room?room.n:"",
                                            icon_path:room?room.ic:"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                                        },
                                        assigned_by_account:{
                                            account_id:parseInt(assigner.aid),
                                            name:assigner?assigner.nm:"",
                                            avatar_image_url:assigner?assigner.av:""
                                        },
                                        message_id:parseInt(task.cid),
                                        body:task.tn,
                                        limit_time:task.lt,
                                        status:task.st
                                    };
                                    added_tasks.push(data);
                                }
                            }

                            var done_tasks=[];
                            tasks = _cwinfo.room_dat[roomid].task_dat;
                            for(var task_id in tasks) {
                                if(!resBody.result.room_dat[roomid].task_dat[task_id]){
                                    var task = tasks[task_id];
                                    var room = _cwinfo.room_dat[roomid];
                                    var account = _cwinfo.contact_dat[task.aid]?_cwinfo.contact_dat[task.aid]:(_cwinfo.account_dat[task.aid]?_cwinfo.account_dat[task.aid]:null);
                                    var assigner = _cwinfo.contact_dat[task.bid]?_cwinfo.contact_dat[task.bid]:(_cwinfo.account_dat[task.bid]?_cwinfo.account_dat[task.bid]:null);
                                    var data={
                                        task_id:parseInt(task_id),
                                        account:{
                                            account_id:parseInt(account.aid),
                                            name:account?account.nm:"",
                                            avatar_image_url:account?account.av:""
                                        },
                                        room:{
                                            room_id:parseInt(roomid),
                                            name:room?room.n:"",
                                            icon_path:room?room.ic:"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                                        },
                                        assigned_by_account:{
                                            account_id:parseInt(assigner.aid),
                                            name:assigner?assigner.nm:"",
                                            avatar_image_url:assigner?assigner.av:""
                                        },
                                        message_id:parseInt(task.cid),
                                        body:task.tn,
                                        limit_time:task.lt,
                                        status:"done"
                                    };
                                    done_tasks.push(data);
                                }
                            }
                            if(params.onmessage) {
                                added_tasks.forEach(function(added_task){
                                    params.onmessage({type:"taskAdded", data:added_task});
                                })
                                done_tasks.forEach(function(done_task){
                                    params.onmessage({type:"taskDone", data:done_task});
                                });
                            }
                            _cwinfo.room_dat[roomid].task_dat = resBody.result.room_dat[roomid].task_dat;
                        }
                    }
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    var getTask = function(params, roomid, taskid) {
        var options = {};
        options.url = 'https://www.chatwork.com/gateway.php?cmd=get_task&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&tid_list%5B%5D='+taskid+'&_='+new Date().getTime().toString();
        options.headers={"Cookie":this._cookies};
        options.method = 'get';
        options.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, options, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    var task = resBody.result.task_dat;
                    var room = _cwinfo.room_dat[roomid];
                    var account = _cwinfo.contact_dat[task[taskid].aid]?_cwinfo.contact_dat[task[taskid].aid]:(_cwinfo.account_dat[task[taskid].aid]?_cwinfo.account_dat[task[taskid].aid]:null);
                    var assigner = _cwinfo.contact_dat[task[taskid].bid]?_cwinfo.contact_dat[task[taskid].bid]:(_cwinfo.account_dat[task[taskid].bid]?_cwinfo.account_dat[task[taskid].bid]:null);
                    var data={
                        task_id:parseInt(taskid),
                        account:{
                            account_id:parseInt(account.aid),
                            name:account?account.nm:"",
                            avatar_image_url:account?account.av:""
                        },
                        room:{
                            room_id:parseInt(roomid),
                            name:room?room.n:"",
                            icon_path:room?room.ic:"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                        },
                        assigned_by_account:{
                            account_id:parseInt(assigner.aid),
                            name:assigner?assigner.nm:"",
                            avatar_image_url:assigner?assigner.av:""
                        },
                        message_id:parseInt(task[taskid].cid),
                        body:task[taskid].tn,
                        limit_time:task[taskid].lt,
                        status:task[taskid].st
                    };

                    if(params.onmessage) {
                        params.onmessage({type:"taskUpdated", data:data});
                    }
                    _cwinfo.room_dat[roomid].task_dat[taskid] = task[taskid];
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    var getChat = function(params, roomid, chatid) {
        var options = {};
        options.url = 'https://www.chatwork.com/gateway.php?cmd=get_chat&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&chat_id='+chatid+'&rid='+roomid;
        options.headers={"Cookie":this._cookies};
        options.method = 'get';
        options.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, options, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    var chat = resBody.result.chat_dat;
                    if(params.onmessage) {
                        var account = (_cwinfo.contact_dat[chat.aid]?_cwinfo.contact_dat[chat.aid]:(_cwinfo.account_dat[chat.aid]?_cwinfo.account_dat[chat.aid]:null))
                        var data = {
                            message_id:parseInt(chat.id),
                            account:{
                                account_id:parseInt(chat.aid),
                                name:account?account.nm:"",
                                avatar_image_url:account?account.av:""
                            },
                            room:{
                                room_id:parseInt(roomid),
                                name:_cwinfo.room_dat[roomid]?_cwinfo.room_dat[roomid].n:"",
                                icon_path:_cwinfo.room_dat[roomid]?_cwinfo.room_dat[roomid].ic:"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                            },
                            body:chat.msg,
                            send_time:chat.tm,
                            update_time:chat.utm
                        }
                        params.onmessage({type:"messageUpdated", data:data});
                    }
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    var getUpdate = function(params) {
        var options = {};
        options.url = 'https://www.chatwork.com/gateway.php?cmd=get_update&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&account_id='+_cwinfo.myid+'&last_id='+_cwinfo.last_id+'&ver=1.80a&new=1&_='+new Date().getTime().toString();
        options.headers={"Cookie":this._cookies};
        options.method = 'post';
        options.headers['Content-Type'] = "application/x-www-form-urlencoded";
        options.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);

        request.call(this, options, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    _cwinfo.last_id = resBody.result.last_id;
                    var rooms = resBody.result.update_info.room;
                    for(var room in rooms) {
                        if(this._roomids!=null && index(this._roomids, room)<0){
                            this._roomids.push(room);
                            _cwinfo.room_dat[room]={};
                        }
                        if(rooms[room].ce!=null) {
                            for(var mid in rooms[room].ce) {
                                getChat.call(this, params, room, mid);
                            }
                        } else if(rooms[room].cd!=null) {
                            for(var mid in rooms[room].cd) {
                                var data = {
                                    id:mid
                                };
                                params.onmessage({type:"messageDeleted", data:data});
                            }
                        } else if(rooms[room].te!=null) {
                            for(var tid in rooms[room].te) {
                                getTask.call(this, params, room, tid);
                            }
                            getRoomInfo.call(this, params, room, rooms[room], false);

                        } else if(rooms[room].td!=null) {
                            for(var taskid in rooms[room].td) {
                                var room = _cwinfo.room_dat[room];
                                var task = room.task_dat[taskid];
                                var account = _cwinfo.contact_dat[task.aid]?_cwinfo.contact_dat[task.aid]:(_cwinfo.account_dat[task.bad]?_cwinfo.account_dat[task.aid]:null);
                                var assigner = _cwinfo.contact_dat[task.bid]?_cwinfo.contact_dat[task.bid]:(_cwinfo.account_dat[task.bid]?_cwinfo.account_dat[task.bid]:null);
                                var data={
                                    task_id:parseInt(taskid),
                                    account:{
                                        account_id:parseInt(account.aid),
                                        name:account?account.nm:"",
                                        avatar_image_url:account?account.av:""
                                    },
                                    room:{
                                        room_id:parseInt(room),
                                        name:room?room.n:"",
                                        icon_path:room?room.ic:"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                                    },
                                    assigned_by_account:{
                                        account_id:parseInt(assigner.aid),
                                        name:assigner?assigner.nm:"",
                                        avatar_image_url:assigner?assigner.av:""
                                    },
                                    message_id:parseInt(task.cid),
                                    body:task.tn,
                                    limit_time:task.lt,
                                    status:"deleted"
                                };
                                params.onmessage({type:"taskDeleted", data:data});
                                delete _cwinfo.room_dat[room].task_dat[taskid];
                            }
//                        } else if(rooms[room].a!=null) {
//                            console.log("invited");
                        } else if(rooms[room].rd!=null) {
                            this._roomids.splice(this._roomids.indexOf(room),1);
                            params.onmessage({
                                type:"roomKicked",
                                data:{
                                    room_id: parseInt(room),
                                    name: _cwinfo.room_dat[room].n,
                                    type: _cwinfo.room_dat[room].tp==1?"group":(_cwinfo.room_dat[room].tp==2?"direct":"my"),
                                    role: _cwinfo.room_dat[room].m[_cwinfo.myid]==1?"admin":(_cwinfo.room_dat[room].m[_cwinfo.myid]==2?"member":"readonly"),
                                    sticky: (_cwinfo.room_dat[room].s==1),
                                    unread_num: -1,
                                    mention_num: -1,
                                    mytask_num: _cwinfo.room_dat[room].mt?_cwinfo.room_dat[room].mt:0,
                                    message_num: _cwinfo.room_dat[room].c,
                                    file_num: _cwinfo.room_dat[room].f,
                                    task_num: _cwinfo.room_dat[room].t,
                                    icon_path: _cwinfo.room_dat[room].ic,
                                    last_update_time: _cwinfo.room_dat[room].lt,
                                    description: _cwinfo.room_dat[room].d?_cwinfo.room_dat[room].d:""
                                }
                            });
                            delete _cwinfo.room_dat[room];
                        } else {
                            getRoomInfo.call(this, params, room, rooms[room]);
                        }
                    }
                } else if (resBody.status.message == "NO LOGIN"){
//                        login(options);
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    var createChannel = function(params) {
        var proxy ="";
        proxy = params.proxy?params.proxy:null;
        var options = { path: require('phantomjs').path };
        if(proxy){
            proxy = Url.parse(proxy);
            options.parameters = {
                proxy:proxy.host,
                "proxy-auth":proxy.auth
            };
//            options.parameters = {proxy:proxy};
        } else if(process.env['HTTPS_PROXY']&&process.env['HTTPS_PROXY']!=""){
            proxy = Url.parse(process.env['https_proxy']);
            options.parameters = {
                proxy:proxy.host,
                "proxy-auth":proxy.auth
            };
        } else if(process.env['https_proxy']&&process.env['https_proxy']!=""){
            proxy = Url.parse(process.env['https_proxy']);
            options.parameters = {
                proxy:proxy.host,
                "proxy-auth":proxy.auth
            };
        }
        phantom.create(options,function(err,ph) {
            return ph.createPage(function(err,page) {
                page.onConsoleMessage = function(msg, lineNum, sourceId) {
                    console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
                };
                page.onError = function(msg, trace) {
                    var msgStack = ['ERROR: ' + msg];
                    console.error(msgStack.join('\n'));
                    params.onerror({name:"PhantomError", message:msgStack.join('\n')});
                }.bind(this);
                page.onLoadFinished = function(status) {
                    // Do other things here...
                };
                return page.open("https://www.chatwork.com/", function(err,status) {
                    page.includeJs('https://talkgadget.google.com/talkgadget/channel.js', function(err) {
                        page.onCallback = function(data) {
                            console.log('event:'+data.event+", data:"+JSON.stringify(data.data));
                            if(data.event == "onmessage") {
                                getUpdate.call(this, params);
                            } else if(data.event == "onclose"||data.event == "onerror") {
                                if(!ph.process.killed){
                                    ph.exit();
                                    setTimeout(function(){
                                    getTokenKey(options);
                                    },30000);
                                }
                            } else if(data.event == "onopen") {
                                this._roomids.forEach(function(roomid){
                                }.bind(this));
                                process.on('exit', function () {
                                    ph.exit();
                                });
                                process.on('uncaughtException', function(err) {
                                    ph.exit();
                                    this.watch(params);
                                }.bind(this));
                                _status = "logged-in";
                                params.oncomplete();
                            }
                        }.bind(this);
                        var token = _cwinfo.token;
                        page.evaluate((function() {
                            var func = function(token) {
                                var onmessage=function(msg){window.callPhantom({event:'onmessage',data:msg});};
                                var onopen = function(){window.callPhantom({event:'onopen',data:'open'});};
                                var onerror = function(){window.callPhantom({event:'onerror',data:'error'});};
                                var onclose = function(){window.callPhantom({event:'onclose', data:'close'});};
                                var channel = new goog.appengine.Channel(token);
                                channel.open({onmessage:onmessage,onopen:onopen,onclose:onclose,onerror:onerror});
                                return channel;
                            };
                            return "function() { return (" + func.toString() + ").apply(this, " + JSON.stringify([token]) + ");}";
                        }()), function(err,result) {
                            if(err&&params.onerror) params.onerror(err, result);
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }.bind(this));
    };
    var _jqjsp = function(response) {
        return response;
    };

    var getToken = function(params) {
        var requestOptions = {};
        requestOptions.url = 'https://ec-chatwork-hrd.appspot.com/token?callback=_jqjsp&myid='+_cwinfo.myid+'&key='+_cwinfo.token_key+'&_'+new Date().getTime().toString()+'=';
        requestOptions.method = 'get';
        requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, requestOptions, {
            "200":function(res, data) {
                var resBody = eval(data);
                if(resBody.status.success == true) {
                    _cwinfo.token = resBody.result.token;
                    createChannel.call(this, params);
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };
    var getTokenKey = function(params) {
        var requestOptions = {};
        requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=get_comet_token_key&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja';
        requestOptions.method = 'post';
        requestOptions.headers={"Cookie":this._cookies};
        requestOptions.headers['Content-Type'] = "application/x-www-form-urlencoded";
        requestOptions.body = "pdata="+encodeURIComponent('{}')
        requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, requestOptions, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    _cwinfo.token_key = resBody.result.token_key;
                    getToken.call(this, params);
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    this.watch = function(params) {
        if(!params.proxy) {
            params.proxy = _initParams.proxy;
        }
        getTokenKey.call(this, params);
    };

    var loadChat = function(params) {
        if(_initParams.onprogress) {
            _initParams.onprogress(this.getStatus()+":start loadChat.");
        }
        var roomid="";
        var last=0;
        var requestOptions = {};
        this._roomids=[];
        for(var room in _cwinfo.room_dat) {
            this._roomids.push(room);
            if(last < _cwinfo.room_dat[room].lt) {
                last = _cwinfo.room_dat[room].lt;
                roomid = room;
            }
        }

        requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=load_chat&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&room_id='+roomid+'&last_chat_id=0&first_chat_id=0&jump_to_chat_id=0&unread_num=0&file=1&desc=1&_='+new Date().getTime().toString();
        requestOptions.headers={"Cookie":this._cookies};
        requestOptions.method = 'get';
        requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, requestOptions, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    _cwinfo.id = resBody.result.chat_list[resBody.result.chat_list.length-1].id;

                    resBody.result.file_list.forEach(function(elm,index,array){
                        var rid = elm.rid;
                        if(_cwinfo.room_dat[rid].finfo==null || _cwinfo.room_dat[rid].finfo.tm<elm.tm) {
                            _cwinfo.room_dat[rid].finfo=elm;
                        }

                    }.bind(this));
                    _status = "logged-in";
                    if(_initParams.oncomplete) {
                        var info={
                            id:parseInt(_cwinfo.myid),
                            name:_cwinfo.myname,
                            myroom_id:parseInt(_cwinfo.myroom),
                            contacts:[],
                            rooms:[]
                        };
                        for(var contact_id in _cwinfo.contact_dat) {
                            info.contacts.push({
                                account_id:parseInt(contact_id),
                                room_id:parseInt(room_id),
                                name:_cwinfo.contact_dat[contact_id].nm,
                                chatwork_id:_cwinfo.contact_dat[contact_id].cwid,
                                facebook_id:_cwinfo.contact_dat[contact_id].fb,
                                twitter_id:_cwinfo.contact_dat[contact_id].tw,
                                skype_id:_cwinfo.contact_dat[contact_id].sp,
                                organization_name:_cwinfo.contact_dat[contact_id].onm,
                                organization_id:parseInt(_cwinfo.contact_dat[contact_id].gid),
                                department:_cwinfo.contact_dat[contact_id].dp,
                                avatar_image_url:_cwinfo.contact_dat[contact_id].av
                            });
                        }
                        var roomids=[];
                        for(var room_id in _cwinfo.room_dat) {
                            info.rooms.push({
                                room_id: parseInt(room_id),
                                name: _cwinfo.room_dat[room_id].n,
                                type: _cwinfo.room_dat[room_id].tp==1?"group":(_cwinfo.room_dat[room_id].tp==2?"direct":"my"),
                                role: _cwinfo.room_dat[room_id].m[_cwinfo.myid]==1?"admin":(_cwinfo.room_dat[room_id].m[_cwinfo.myid]==2?"member":"readonly"),
                                sticky: (_cwinfo.room_dat[room_id].s==1),
                                unread_num: -1,
                                mention_num: -1,
                                mytask_num: _cwinfo.room_dat[room_id].mt?_cwinfo.room_dat[room_id].mt:0,
                                message_num: _cwinfo.room_dat[room_id].c,
                                file_num: _cwinfo.room_dat[room_id].f,
                                task_num: _cwinfo.room_dat[room_id].t,
                                icon_path: _cwinfo.room_dat[room_id].ic,
                                last_update_time: _cwinfo.room_dat[room_id].lt
//                                description: _cwinfo.room_dat[room_id].d?_cwinfo.room_dat[room_id].d:""
                            });
                            roomids.push(room_id);
                        }
                        var load_tasks_chat = function(roomid){
                            var options={};
                            options.url = "https://www.chatwork.com/gateway.php?cmd=load_chat&myid="+_cwinfo.myid+"&_v=1.80a&_av=4&_t="+_cwinfo.ACCESS_TOKEN+"&ln=ja&room_id="+roomid+"&last_chat_id=0&first_chat_id=0&jump_to_chat_id=0&unread_num=0&load_file_version=2&task=1&desc=1&_="+new Date().getTime().toString();
                            options.headers={"Cookie":this._cookies};
                            options.method = 'get';
                            options.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
                            request.call(this, options, {
                                "200":function(res, data) {
                                    var resBody = JSON.parse(data);
                                    if(roomids.length==0) {
                                        if(params&&params.reload&&params.oncomplete) {
                                            params.oncomplete();
                                        }else if(_initParams.oncomplete) {
                                            _initParams.oncomplete(info);
                                        }
                                    } else {
                                        if(resBody.status.success == true) {
                                            _cwinfo.room_dat[roomid].task_dat = resBody.result.task_dat;
                                        }
                                        load_tasks_chat(roomids.shift());
                                    }
                                },
                                "else":function(res, data) {
                                    if(roomids.length==0) {
                                        if(params&&params.reload&&params.oncomplete) {
                                            params.oncomplete();
                                        }else if(_initParams.oncomplete) {
                                            _initParams.oncomplete(info);
                                        }
                                    } else {
                                        load_tasks_chat(roomids.shift());
                                    }
                                }
                            });
                        }.bind(this);
                        load_tasks_chat(roomids.shift());
                    }
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params&&params.reload&&params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    } else if(_initParams.onerror){
                        _initParams.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params&&params.reload&&params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                } else if(_initParams.onerror){
                    _initParams.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };
    var getAccountInfo = function(params) {
        if(_initParams.onprogress) {
            _initParams.onprogress(this.getStatus()+":start getAccountInfo.");
        }
        var requestOptions = {};
        requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=get_account_info&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja';
        requestOptions.headers={"Cookie":this._cookies};
        requestOptions.headers["Content-Type"]="application/x-www-form-urlencoded";
        requestOptions.method = 'post';
        requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        var unknowns=[];
        for(var roomid in _cwinfo.room_dat){
            for(var aid in _cwinfo.room_dat[roomid].m){
                if(!_cwinfo.contact_dat[aid]&&unknowns.indexOf(aid)<0){
                    unknowns.push(parseInt(aid));
                }
            }
        }
        requestOptions.body = "pdata="+encodeURIComponent('{"aid":['+unknowns+'],"get_private_data":0}');

        request.call(this, requestOptions, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    _cwinfo.account_dat = resBody.result.account_dat;
                    for(var contactid in _cwinfo.account_dat) {
                        _cwinfo.account_dat[contactid].av = "https://tky-chat-work-appdata.s3.amazonaws.com/avatar/"+_cwinfo.account_dat[contactid].av;
                    }
                    loadChat.call(this, params);
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(_initParamsonerror){
                        _initParams.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                _cwinfo.account_dat={};
                loadChat.call(this, params);
//                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
//                if(_initParams.onerror){
//                    _initParams.onerror({name:"LoginError", message:msg});
//                }
            }.bind(this)
        });
    };
    var initLoad = function(params) {
        if(_initParams.onprogress) {
            _initParams.onprogress(this.getStatus()+":start initLoad.");
        }
        var requestOptions = {};
        requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=init_load&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&rid=0&type=&new=1';
        requestOptions.headers={"Cookie":this._cookies};
        requestOptions.method = 'get';
        requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, requestOptions, {
            "200":function(res, data) {
                var resBody = JSON.parse(data);
                if(resBody.status.success == true) {
                    _cwinfo.last_id = resBody.result.last_id;
                    _cwinfo.room_dat = resBody.result.room_dat;
                    _cwinfo.contact_dat = resBody.result.contact_dat;
                    for(var contactid in _cwinfo.contact_dat) {
                        _cwinfo.contact_dat[contactid].av = "https://tky-chat-work-appdata.s3.amazonaws.com/avatar/"+_cwinfo.contact_dat[contactid].av;
                    }
                    for(var roomid in _cwinfo.room_dat) {
                        var room = _cwinfo.room_dat[roomid];
                        _cwinfo.room_dat[roomid].u=0;
                        var count=0, another_id=null;
                        for(var aid in room.m) {
                            count++;
                            if(aid != _cwinfo.myid) {
                                another_id = aid;
                            }
                        }
                        if(count == 1&&room.tp==3) {
                            _cwinfo.myroom = roomid;
                            _cwinfo.room_dat[roomid].n=_cwinfo.contact_dat[_cwinfo.myid].nm;
                            _cwinfo.room_dat[roomid].ic = _cwinfo.contact_dat[_cwinfo.myid].av;
                        }else if(count == 2&&room.tp==2){
                            _cwinfo.room_dat[roomid].n = _cwinfo.contact_dat[another_id].nm;
                            _cwinfo.room_dat[roomid].ic = _cwinfo.contact_dat[another_id].av;
                        }else if(typeof _cwinfo.room_dat[roomid].ic == "string"){
                            _cwinfo.room_dat[roomid].ic = "https://tky-chat-work-appdata.s3.amazonaws.com/icon/"+_cwinfo.room_dat[roomid].ic;
                        }else if(typeof _cwinfo.room_dat[roomid].ic == "number"){
                            _cwinfo.room_dat[roomid].ic = _cwinfo.icon_preset[_cwinfo.room_dat[roomid].ic-1];
                        }else if(!_cwinfo.room_dat[roomid].ic) {
                            _cwinfo.room_dat[roomid].ic = "https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png";
                        }
                    }
                    _cwinfo.myname = resBody.result.contact_dat[_cwinfo.myid].nm;
                    if(params&&params.reload&&params.oncomplete){
                        loadChat.call(this,params);
                    }else{
                        getAccountInfo.call(this, params);
                    }
                } else {
                    var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                    if(params&&params.reload&&params.onerror){
                        params.onerror({name:"UnknownError", message:msg});
                    }else if(_initParams.onerror){
                        _initParams.onerror({name:"UnknownError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(params&&params.reload&&params.onerror){
                    params.onerror({name:"LoginError", message:msg});
                } else if(_initParams.onerror){
                    _initParams.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    var getSession = function(params){
        if(_initParams.onprogress) {
            _initParams.onprogress(this.getStatus()+":start getSession.");
        }
        var requestOptions={};
        requestOptions.url = 'https://www.chatwork.com/';
        requestOptions.headers={"Cookie":this._cookies};
        requestOptions.method = 'get';
        requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
        request.call(this, requestOptions, {
            "200":function(res, data) {
                var myid = "", ACCESS_TOKEN="", icon_preset=[];
                if(data.match(/var myid = '([^']+)';/)!=null) {
                    myid=data.match(/var myid = '([^']+)';/)[1];
                }
                if(data.match(/var ACCESS_TOKEN = '([^']+)';/)!=null) {
                    ACCESS_TOKEN=data.match(/var ACCESS_TOKEN = '([^']+)';/)[1];
                }
                preset_str=data.match(/<ul id="_roomInfoIconPreset">\s*(?:<li>\s*<img [^>]+>\s*<\/li>\s*)+<\/ul>/);
                if(preset_str) {
                    preset_str=preset_str[0].match(/src="[^"]*"/g);
                    preset_str.forEach(function(src){
                        icon_preset.push(src.replace(/src="([^"]*)"/,"$1"));
                    })
                }
                if(myid != "" && ACCESS_TOKEN != "") {
                    _cwinfo = {myid:myid, ACCESS_TOKEN:ACCESS_TOKEN, icon_preset:icon_preset};
                    initLoad.call(this, params);
                } else {
                    var msg = "myid and ACCESS_TOKEN are null.";
                    if(_initParams.onerror){
                        _initParams.onerror({name:"LoginError", message:msg});
                    }
                }
            }.bind(this),
            "else":function(res, data) {
                var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                if(_initParams.onerror){
                    _initParams.onerror({name:"LoginError", message:msg});
                }
            }.bind(this)
        });
    };

    var _initParams={};
    this.init = function(params) {
        _cwinfo = null;
        if(params) {
            _initParams = {};
            for(var name in params) {
                _initParams[name] = params[name];
            }
            this._email = params.email;
            this._password = params.password;

            this._cookies = "";
            _cwinfo = {};
            if(_initParams.onprogress) {
                _initParams.onprogress(this.getStatus()+":start init.");
            }

            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/login.php?lang=ja&args=';
            requestOptions.headers={"Content-Type":"application/x-www-form-urlencoded"};
            requestOptions.method = 'post';
            requestOptions.body = "email="+encodeURIComponent(this._email)+"&password="+encodeURIComponent(this._password)+"&auto_login=on&login="+encodeURIComponent("ログイン");
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
            request.call(this, requestOptions, {
                "302":function(res, data) {
                    var cookies = {};
                    res.headers['set-cookie'].forEach(function(cookie){
                        if(cookie.match(/cwssid=([0-9a-z]{32});/)!=null) {
                            cookies["cwssid"]= cookie.match(/cwssid=([0-9a-z]{32});/)[1];
                        } else if(cookie.match(/AWSELB=([^;]+);/)!=null) {
                            cookies["AWSELB"] = cookie.match(/AWSELB=([^;]+);/)[1];
                        } else if(cookie.match(/auto_logindefault=([^;]+);/)!=null) {
                            cookies["auto_logindefault"] = cookie.match(/auto_logindefault=([^;]+);/)[1];
                        }
                    });
                    this._cookies = "";
                    for(var name in cookies){
                        this._cookies += name+"="+cookies[name]+"; ";
                    }

                    getSession.call(this, params);
                }.bind(this),
                "else":function(res, data) {
                    _status = null;
                    var msg="";
                    if(/ログイン情報が間違っています/.test(data)){
                        msg = "Invalid email or password."
                    } else {
                        msg = "Unknown error."
                    }
                    if(_initParams.onerror){
                        _initParams.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        }
    };

    // current status
    this.getMembers = function(params) {
        initLoad.call(this, {
            reload:true,
            oncomplete:function(){
                var room = _cwinfo.room_dat[params.room_id];
                var members = [];
                for (var member_id in room.m) {
                    var member = _cwinfo.contact_dat[member_id]?_cwinfo.contact_dat[member_id]:(_cwinfo.account_dat[member_id]?_cwinfo.account_dat[member_id]:null);
                    if(!member)continue;

                    members.push({
                        account_id:parseInt(member_id),
                        name:member.nm,
                        chatwork_id:member.cwid,
                        organization_name:member.onm,
                        organization_id:parseInt(member.gid),
                        department:member.dp,
                        avatar_image_url:member.av
                    })
                }
                if(params.oncomplete) {
                    params.oncomplete(members);
                }
            },
            onerror:function(err) {
                if(params.onerror){
                    params.onerror(err);
                }
            }
        })
    };
    this.getRoom = function(params) {
        this.getRooms({
            _myroom:true,
            oncomplete:function(rooms){
                var room=null;
                rooms.some(function(elm){
                    if(elm.room_id==params.room_id){
                        room = elm;
                        return true;
                    }
                    return false;
                })
                if(params.oncomplete) {
                    params.oncomplete(room);
                }
            },
            onerror:function(err) {
                if(params.onerror){
                    params.onerror(err);
                }
            }
        })
    };
    this.getRooms = function(params) {
        initLoad.call(this, {
            reload:true,
            oncomplete:function(){
                var rooms=[];
                for(var room_id in _cwinfo.room_dat) {
                    var room = _cwinfo.room_dat[room_id];
                    if(!params._myroom&&room.tp==3||!room)continue;
                    rooms.push({
                        room_id: parseInt(room_id),
                        name: room.n,
                        type: room.tp==1?"group":(room.tp==2?"direct":"my"),
                        role: room.m[_cwinfo.myid]==1?"admin":(room.m[_cwinfo.myid]==2?"member":"readonly"),
                        sticky: (room.s==1),
                        unread_num: -1,
                        mention_num: -1,
                        mytask_num: room.mt?room.mt:0,
                        message_num: room.c,
                        file_num: room.f,
                        task_num: room.t,
                        icon_path: room.ic,
                        last_update_time: room.lt,
                        description: room.d?room.d:""
                    });
                }
                if(params.oncomplete) {
                    params.oncomplete(rooms);
                }
            },
            onerror:function(err) {
                if(params.onerror){
                    params.onerror(err);
                }
            }
        })
    };
    this.getContacts = function(params) {
        initLoad.call(this, {
            reload:true,
            oncomplete:function(){
                var contacts=[];
                for(var contact_id in _cwinfo.contact_dat) {
                    if(contact_id==_cwinfo.myid)continue;
                    var contact = _cwinfo.contact_dat[contact_id];
                    if(!contact)continue;
                    var room_id;
                    for(room_id in _cwinfo.room_dat) {
                        if(_cwinfo.room_dat[room_id].tp == 2 && _cwinfo.room_dat[room_id].m[contact_id]) {
                            break;
                        }
                    }
                    contacts.push({
                        account_id:parseInt(contact_id),
                        room_id:parseInt(room_id),
                        name:contact.nm,
                        chatwork_id:contact.cwid,
                        organization_name:contact.onm,
                        organization_id:parseInt(contact.gid),
                        department:contact.dp,
                        avatar_image_url:contact.av
                    })
                }
                if(params.oncomplete) {
                    params.oncomplete(contacts);
                }
            },
            onerror:function(err) {
                if(params.onerror){
                    params.onerror(err);
                }
            }
        });
    };

    this.getDetailMyInfo = function(params) {
        if(params&&_status){
            params.aid = _cwinfo.myid;
            this.getDetailAccountInfo({
                aid:_cwinfo.myid,
                oncomplete:function(account_dat) {
                    account_dat.rooms=[];
                    account_dat.contacts=[];
                    account_dat.myroomid=parseInt(_cwinfo.myroom);
                    var contacts={};
                    for(var contact_id in _cwinfo.contact_dat) {
                        contacts[contact_id]={
                            id:contact_id,
                            name:_cwinfo.contact_dat[contact_id].nm,
                            chatwork_id:_cwinfo.contact_dat[contact_id].cwid,
                            facebook:_cwinfo.contact_dat[contact_id].fb,
                            twitter:_cwinfo.contact_dat[contact_id].tw,
                            skype:_cwinfo.contact_dat[contact_id].sp,
                            organization_name:_cwinfo.contact_dat[contact_id].onm,
                            organization_id:_cwinfo.contact_dat[contact_id].gid,
                            department:_cwinfo.contact_dat[contact_id].dp,
                            avatar_image_url:_cwinfo.contact_dat[contact_id].av
                        }
                        account_dat.contacts.push(contacts[contact_id]);
                    }
                    var accounts={};
                    for(var account_id in _cwinfo.account_dat) {
                        accounts[account_id]={
                            name:_cwinfo.account_dat[account_id].nm,
                            chatwork_id:_cwinfo.account_dat[account_id].cwid,
                            facebook:_cwinfo.account_dat[account_id].fb,
                            twitter:_cwinfo.account_dat[account_id].tw,
                            skype:_cwinfo.account_dat[account_id].sp,
                            organization_name:_cwinfo.account_dat[account_id].onm,
                            organization_id:_cwinfo.account_dat[account_id].gid,
                            department:_cwinfo.account_dat[account_id].dp,
                            avatar_image_url:_cwinfo.account_dat[account_id].av
                        }
                    }
                    for(var room_id in _cwinfo.room_dat) {
                        var room = {
                            id:room_id,
                            type:_cwinfo.room_dat[room_id].n?"group":"direct",
                            name:_cwinfo.room_dat[room_id].n?_cwinfo.room_dat[room_id].n:"",
                            members:[]
                        };
                        var mem_count = 0;
                        var mem_name = "";
                        for (var member_id in _cwinfo.room_dat[room_id].m) {
                            mem_count++;
                            if(member_id != _cwinfo.myid){
                                mem_name = contacts[member_id]?contacts[member_id].name:"";
                            }
                            var member = contacts[member_id]?contacts[member_id]:(accounts[member_id]?accounts[member_id]:null);
                            if(member) {
                                room.members[member_id] = {
                                    name:member.name
                                };
                            }else{
                                room.members[member_id] = null;
                            }
                        }
                        if(mem_count==1) {
                            room.name = "my chat";
                        }else if(mem_count==2 && room.name == ""){
                            room.name = mem_name;
                        }
                        account_dat.rooms.push(room);
                    }
                    this.getMyTasks({
                        status:"open",
                        oncomplete:function(open_tasks_dat){
                            account_dat.tasks={
                                open:[]
                            };
                            for (var open_task_id in open_tasks_dat){
                                open_tasks_dat[open_task_id].id = open_task_id;
                                account_dat.tasks.open.push(open_tasks_dat[open_task_id]);
                            }
                            if(!params.include_done){
                                if(params&&params.oncomplete) {
                                    params.oncomplete(account_dat);
                                }
                                return;
                            }
                            this.getMyTasks({
                                status:"done",
                                oncomplete:function(done_tasks_dat){
                                    account_dat.tasks.done=[];
                                    for (var done_task_id in done_tasks_dat){
                                        done_tasks_dat[done_task_id].id = open_task_id;
                                        account_dat.tasks.done.push(done_tasks_dat[done_task_id]);
                                    }
                                    if(params&&params.oncomplete) params.oncomplete(account_dat);
                                },
                                onerror:function(err) {
                                    if(params&&params.onerror) params.onerror(err);
                                }
                            })
                        }.bind(this),
                        onerror:function(err) {
                            if(params&&params.onerror) params.onerror(err);
                        }
                    })
                }.bind(this),
                onerror:function(err){
                    if(params&&params.onerror) params.onerror(err);
                }
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    };

    this.updateRoom = function(params) {
        if(params&&_status){
            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=update_room&_f=1&&myid='+_cwinfo.myid+'&_v=1.80a&_t='+_cwinfo.ACCESS_TOKEN;
            requestOptions.headers={"Cookie":this._cookies};
            var boundary = "----NodeFormBoundary" + Math.random().toString(16);
            requestOptions.headers["Content-Type"]="multipart/form-data; boundary="+boundary;
            requestOptions.method = 'post';
            requestOptions.body =
                (params.room.name?'--'+boundary+'\r\nContent-Disposition: form-data; name="__cwform_name"\r\n\r\n'+params.room.name+'\r\n':'') +
                (params.room.description?'--'+boundary+'\r\nContent-Disposition: form-data; name="__cwform_description"\r\n\r\n'+params.room.description+'\r\n':'') +
                (params.room.iconfile?'':'') + // icon updating is not supported.
                '--'+boundary+'\r\nContent-Disposition: form-data; name="__cwform_room_id"\r\n\r\n'+params.room.room_id+'\r\n--'+boundary+'--\r\n';
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);

            request.call(this, requestOptions, {
                "200":function(res, data) {
                    var resBody = JSON.parse(data.replace(/&quot;/g,"\""));
                    if(resBody.status.success == true) {
                        if(params.oncomplete){
                            params.oncomplete(resBody.result);
                        }
                    } else {
                        var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                        if(params.onerror){
                            params.onerror({name:"UnknownError", message:msg});
                        }
                    }
                }.bind(this),
                "else":function(res, data) {
                    console.log(res.statusCode);
                    var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                    if(params.onerror){
                        params.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    }
    this.getDetailAccountInfo = function(params) {
        if(params&&_status){
            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=get_detail_account_info&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja';
            requestOptions.headers={"Cookie":this._cookies};
            requestOptions.headers["Content-Type"]="application/x-www-form-urlencoded";
            requestOptions.method = 'post';
            requestOptions.body = "pdata="+encodeURIComponent('{"aid":"'+(params.aid?params.aid:_cwinfo.myid)+'","get_private_data":0}');
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);

            request.call(this, requestOptions, {
                "200":function(res, data) {
                    var resBody = JSON.parse(data);
                    if(resBody.status.success == true) {
                        if(params.oncomplete){
                            var data={
                                account_id:parseInt(resBody.result.account_dat.aid),
                                name:resBody.result.account_dat.nm,
                                chatwork_id:resBody.result.account_dat.cwid,
                                organization_id:resBody.result.account_dat.gid,
                                organization_name:resBody.result.account_dat.onm,
                                department:resBody.result.account_dat.dp,
                                title:resBody.result.account_dat.tt,
                                url:resBody.result.account_dat.url,
                                introduction:resBody.result.account_dat.intro,
                                mail:resBody.result.account_dat.mail,
                                tel_organization:resBody.result.account_dat.otel,
                                tel_extension:resBody.result.account_dat.etel,
                                tel_mobile:resBody.result.account_dat.mtel,
                                skype:resBody.result.account_dat.sp,
                                twitter:resBody.result.account_dat.tw,
                                facebook:resBody.result.account_dat.fb,
                                avatar_image_url:"https://tky-chat-work-appdata.s3.amazonaws.com/avatar/"+resBody.result.account_dat.av,
                                address:resBody.result.account_dat.ad,
                                cover_image_url:"https://tky-chat-work-appdata.s3.amazonaws.com/cover/"+resBody.result.account_dat.cv,
                            }
                            params.oncomplete(data);
                        }
                    } else {
                        var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                        if(params.onerror){
                            params.onerror({name:"UnknownError", message:msg});
                        }
                    }
                }.bind(this),
                "else":function(res, data) {
                    var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                    if(params.onerror){
                        params.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    }
    this.getStatus = function() {
        return _status;
    };

    // chat-operations
    this.getMessage = function(params) {
        getChat.call(this,
            {
                onmessage:function(data){
                    if(data.type == "chatUpdated"&&params.oncomplete){
                        params.oncomplete(data.data);
                    }
                },
                onerror:function(err) {
                    if(params.onerror){
                        params.onerror(err);
                    }
                }
            },
            params.room_id,
            params.message_id
        );
    };
    this.sendMessage = function(params){
        if(params&&_status){
            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=send_chat&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja';
            requestOptions.headers={"Cookie":this._cookies};
            requestOptions.headers["Content-Type"]="application/x-www-form-urlencoded";
            requestOptions.method = 'post';
            requestOptions.body = "pdata="+encodeURIComponent('{"text":"'+params.msg.text+'","room_id":"'+params.msg.room_id+'","last_chat_id":0,"read":1,"edit_id":0}');
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);


            request.call(this, requestOptions, {
                "200":function(res, data) {
                    var resBody = JSON.parse(data);
                    if(resBody.status.success == true) {
                        if(params.oncomplete){
                            var data={};
                            data={
                                message_id:resBody.result.new_message_id
                            }
                            params.oncomplete(resBody.result.account_dat);
                        }
                    } else {
                        var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                        if(params.onerror){
                            params.onerror({name:"UnknownError", message:msg});
                        }
                    }
                }.bind(this),
                "else":function(res, data) {
                    var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                    if(params.onerror){
                        params.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    };
/*
    this.editMessage = function(params){
        if(params&&params.onerror&&!_status) {
            var msg = "not implemented.";
            if(params.onerror){
                params.onerror({name:"UnknownError", message:msg});
            }
        }
        return false;
    };
    this.removeMessage = function(params){
        if(params&&params.onerror&&!_status) {
            var msg = "not implemented.";
            if(params.onerror){
                params.onerror({name:"UnknownError", message:msg});
            }
        }
        return false;
    };
*/
    // task-operations
    this.getTask = function(params) {
        getTask.call(this,
            {
                onmessage:function(data){
                    if(data.type == "taskUpdated"&&params.oncomplete){
                        params.oncomplete(data.data);
                    }
                },
                onerror:function(err) {
                    if(params.onerror){
                        params.onerror(err);
                    }
                }
            },
            params.room_id,
            params.task_id
        );
    };
    this.getMyTasks = function(params) {
        if(params&&_status){
            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=load_my_task&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&type=my&status='+params.status;
            requestOptions.headers={"Cookie":this._cookies};
            requestOptions.method = 'get';
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);

            request.call(this, requestOptions, {
                "200":function(res, data) {
                    var resBody = JSON.parse(data);
                    if(resBody.status.success == true) {
                        if(params.oncomplete){
                            var data=[];
                            for(var task_id in resBody.result.task_dat) {
                                var task=resBody.result.task_dat[task_id];
                                var room_id=parseInt(task.rid);
                                var account = _cwinfo.contact_dat[task.aid]?_cwinfo.contact_dat[task.aid]:(_cwinfo.account_dat[task.aid]?_cwinfo.account_dat[task.aid]:null);
                                var assigner = _cwinfo.contact_dat[task.bid]?_cwinfo.contact_dat[task.bid]:(_cwinfo.account_dat[task.bid]?_cwinfo.account_dat[task.bid]:null);
                                var datum={
                                    task_id:parseInt(task_id),
                                    account:{
                                        account_id:account?parseInt(account.aid):-1,
                                        name:account?account.nm:"",
                                        avatar_image_url:account?account.av:""
                                    },
                                    room:{
                                        room_id:parseInt(room_id),
                                        name:_cwinfo.room_dat[room_id].n,
                                        icon_path:(_cwinfo.room_dat[room_id].ic
                                            ?_cwinfo.room_dat[room_id].ic
                                            :"https://tky-chat-work-appdata.s3.amazonaws.com/icon/ico_group.png"
                                        )
                                    },
                                    assigned_by_account:{
                                        account_id:parseInt(assigner.aid),
                                        name:assigner?assigner.nm:"",
                                        avatar_image_url:assigner?assigner.av:""
                                    },
                                    message_id:parseInt(task.cid),
                                    body:task.tn,
                                    limit_time:task.lt,
                                    status:task.st
                                };
                                data.push(datum);
                            }
                            params.oncomplete(data);
                        }
                    } else {
                        var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                        if(params.onerror){
                            params.onerror({name:"UnknownError", message:msg});
                        }
                    }
                }.bind(this),
                "else":function(res, data) {
                    var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                    if(params.onerror){
                        params.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    };
    this.addTask = function(params){
        if(params&&_status){
            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=add_task&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja';
            requestOptions.headers={"Cookie":this._cookies};
            requestOptions.headers["Content-Type"]="application/x-www-form-urlencoded";
            requestOptions.method = 'post';
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
            var task_limit = null;
            if(params.task.limit) {
                task_limit = parseInt(new Date(params.task.limit.year, params.task.limit.month-1, params.task.limit.date+1).getTime()/1000) - 1;
            } else {
                var today = new Date();
                task_limit = parseInt(today.getTime()/86400000+2)*86400+today.getTimezoneOffset()*60-1
            }
            requestOptions.body = "pdata="+encodeURIComponent('{"task":"'+params.task.body+'","task_limit":'+task_limit+',"assign":["'+params.task.to_ids.toString().replace(/,/g,"\",\"")+'"],"room_id":"'+params.task.room_id+'"}');


            request.call(this, requestOptions, {
                "200":function(res, data) {
                    var resBody = JSON.parse(data);
                    if(resBody.status.success == true) {
                        if(params.oncomplete){
                            params.oncomplete(resBody.status.success);
                        }
                    } else {
                        var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                        if(params.onerror){
                            params.onerror({name:"UnknownError", message:msg});
                        }
                    }
                }.bind(this),
                "else":function(res, data) {
                    var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                    if(params.onerror){
                        params.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    };
    this.closeTask = function(params){
        if(params&&_status){
            var requestOptions={};
            requestOptions.url = 'https://www.chatwork.com/gateway.php?cmd=check_task&myid='+_cwinfo.myid+'&_v=1.80a&_av=4&_t='+_cwinfo.ACCESS_TOKEN+'&ln=ja&task_id='+params.task_id+'&to=1&_='+new Date().getTime();
            requestOptions.headers={"Cookie":this._cookies};
            requestOptions.method = 'get';
            requestOptions.proxy = params.proxy?params.proxy:(_initParams.proxy?_initParams.proxy:null);
            request.call(this, requestOptions, {
                "200":function(res, data) {
                    var resBody = JSON.parse(data);
                    if(resBody.status.success == true) {
                        if(params.oncomplete){
                            params.oncomplete(resBody.status.success);
                        }
                    } else {
                        var msg = "UnknownStatus: " + JSON.stringify(resBody.status) + ".";
                        if(params.onerror){
                            params.onerror({name:"UnknownError", message:msg});
                        }
                    }
                }.bind(this),
                "else":function(res, data) {
                    var msg = "StatusCode is not 200, but " + res.statusCode + ".";
                    if(params.onerror){
                        params.onerror({name:"LoginError", message:msg});
                    }
                }.bind(this)
            });
        } else if(params&&params.onerror&&!_status) {
            var msg = "not logged-in.";
            if(params.onerror){
                params.onerror({name:"LoginError", message:msg});
            }
            return false;
        } else if(_status) {
            return true;
        } else {
            return false;
        }
    };
/*
    this.editTask = function(params){
        if(params&&params.onerror&&!_status) {
            var msg = "not implemented.";
            if(params.onerror){
                params.onerror({name:"UnknownError", message:msg});
            }
        }
        return false;
    };
    this.removeTask = function(params){
        if(params&&params.onerror&&!_status) {
            var msg = "not implemented.";
            if(params.onerror){
                params.onerror({name:"UnknownError", message:msg});
            }
        }
        return false;
    };
*/
};

module.exports = Chatwork;

function createHttpRequest(options) {
    var url = Url.parse( options.url, false );
    var httpModel=null;
    var envPrefix="";
    if( url.protocol == "http:") {
        if(!url.port ) url.port = 80;
        httpModel = http;
        envPrefix = "HTTP";
    } else if( url.protocol == "https:"){
        if(!url.port ) url.port = 443;
        httpModel = https;
        envPrefix = "HTTPS";
    }
    var requestOptions = {
        method: options.method
      , headers:options.headers
    };
    requestOptions.hostname = url.hostname;
    requestOptions.port = url.port;
    requestOptions.path = url.path;
    try {
        var proxy = null;
        if(options.proxy) {
            proxy= Url.parse( options.proxy, false );
        } else if(process.env[envPrefix+'_PROXY']&&process.env[envPrefix+'_PROXY']!=""){
            proxy = Url.parse(process.env[envPrefix+'_PROXY']);
        } else if(process.env[envPrefix.toLowerCase()+'_proxy']&&process.env[envPrefix.toLowerCase()+'_proxy']!=""){
            proxy = Url.parse(process.env[envPrefix.toLowerCase()+'_proxy']);
        }
        if(proxy) {
            var proxyauth = proxy.auth;
            if(proxy != null && httpModel == http && proxy.protocol === "http:") {
                requestOptions.headers["Host"] = url.host;
                requestOptions.path = url.protocol + "//"+url.host+url.path;
                requestOptions.port = proxy.port;
                requestOptions.host = proxy.hostname;
                if(proxyauth != null) {
                   requestOptions.headers['Proxy-Authorization'] = 'Basic ' + new Buffer(proxyauth.user+":"+proxyauth.password).toString('base64');
                };
            } else if(proxy != null && httpModel == https && proxy.protocol === "http:") {
                var agentOption = {
                    host: proxy.hostname,
                    port: proxy.port,
                    proxyAuth: (proxyauth?proxyauth:null)
                };
                requestOptions.agent = Tunnel.httpsOverHttp({proxy:agentOption});
            }
        }
    } catch (e) {
        console.log(0,e)
    }
    return httpModel.request(requestOptions);
}
var index = (function (undefined) {
    var f;
    return function (arr, val, from) {
        if (from !== undefined) {
            if (from < 0) {
                f = from + arr.length;
                if (f < 0)
                    return -1;
            } else {
                f = from;
            }
        } else {
            f = 0;
        }
        for (var i=f; i<arr.length; i++) {
            if (arr[i] === val) {
                return i;
            }
        }
        return -1;
    };
})();
